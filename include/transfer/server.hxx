#ifndef UDP_TRASMITTER_RECIEVER_SERVER_HXX
#define UDP_TRASMITTER_RECIEVER_SERVER_HXX

#include "application.hxx"
namespace transfer
{
  namespace udp
  {
    class Server : public transfer::udp::Application
    {
     public:
      Server(const Params& params);

      int DoRun() override;

      void *readfile();

        virtual ~Server();

    private:
        int fileDesc;// file descriptor
        size_t filesize;
    };
  }  // namespace udp
}  // namespace transfer
#endif  // UDP_TRASMITTER_RECIEVER_SERVER_HXX
