#ifndef UDP_TRASMITTER_RECIEVER_CLIENT_HXX
#define UDP_TRASMITTER_RECIEVER_CLIENT_HXX

#include "application.hxx"
#include "packet.hxx"
#include "Connection.hxx"

namespace transfer
{
  namespace udp
  {
    class Client : public transfer::udp::Application
    {
     public:
      Client(const Params& params);

      int DoRun() override;

      void initfile(const char* filename);

        virtual ~Client();

    private:
        int fileDesc = -1;
        void* map = nullptr;
        size_t filelen = 0;
        static constexpr size_t BUFFER_LEN = 1500;/*MTU size*/
    private:
        void ConfirmAndRequestNewPieceOfData(std::unique_ptr<transfer::udp::Connection> &connection, size_t packet_num) const;

        void ReceiveDataPacket(std::unique_ptr<transfer::udp::Connection> &connection, transfer::udp::Packet &packet) const;
    };
  }  // namespace udp
}  // namespace transfer
#endif  // UDP_TRASMITTER_RECIEVER_CLIENT_HXX
