#include <unistd.h>
#include <netdb.h>
#include <iostream>
#include <cstring>
#include "transfer/UdpConnection.hxx"

namespace transfer
{
  namespace udp
  {
    int UdpConnection::read(void* buf, const size_t len)
    {
      if (isValid()) {
        return ::recvfrom(socketFd_,
                          buf,
                          len,
                          MSG_WAITALL,
                          reinterpret_cast<struct sockaddr*>(&clientAddr),
                          &clientAddrLen);
      }
      return -1;
    }
    int UdpConnection::write(const void* data, const size_t len)
    {
      ::sendto(socketFd_,
               data,
               len,
               MSG_CONFIRM,
               reinterpret_cast<const struct sockaddr*>(&clientAddr),
               clientAddrLen);
      if (isValid()) { return ::write(socketFd_, data, len); }
      return -1;
    }
    bool UdpConnection::isValid() const
    {
      return socketFd_ != -1;
    }
    void UdpConnection::connect()
    {
      struct addrinfo hints;
      struct addrinfo *result, *rp;
      int sfd, s;

      /* Obtain address(es) matching host/port */
      hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
      hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
      hints.ai_flags = 0;
      hints.ai_protocol = 0; /* Any protocol */

      serverAddrLen = sizeof(serverAddr);
      memcpy(&serverAddr, &hints, sizeof(serverAddr));

      s = getaddrinfo(
          hostname_.c_str(), std::to_string(portNum_).c_str(), &hints, &result);
      if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
      }

      /* getaddrinfo() returns a list of address structures.
         Try each address until we successfully connect(2).
         If socket(2) (or connect(2)) fails, we (close the socket
         and) try the next address. */

      for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd != -1) break;
      }

      if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not connect\n");
        exit(EXIT_FAILURE);
      }

      clientAddrLen = rp->ai_addrlen;
      memcpy(&clientAddr, rp->ai_addr, clientAddrLen);
      freeaddrinfo(result); /* No longer needed */
      socketFd_ = sfd;
    }
    UdpConnection::~UdpConnection()
    {
      if (isValid()) { close(socketFd_); }
    }
    int UdpConnection::bind()
    {
      struct addrinfo hints = {};
      struct addrinfo *result, *rp;
      int sfd, s;
      struct sockaddr_storage peer_addr;
      socklen_t peer_addr_len;

      hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
      hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
      hints.ai_flags = AI_PASSIVE; /* For wildcard IP address */
      hints.ai_protocol = 0; /* Any protocol */
      hints.ai_canonname = NULL;
      hints.ai_addr = NULL;
      hints.ai_next = NULL;

      serverAddrLen = sizeof(serverAddr);
      memcpy(&serverAddr, &hints, sizeof(serverAddr));

      s = getaddrinfo(
          hostname_.c_str(), std::to_string(portNum_).c_str(), &hints, &result);
      if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
      }

      /* getaddrinfo() returns a list of address structures.
         Try each address until we successfully bind(2).
         If socket(2) (or bind(2)) fails, we (close the socket
         and) try the next address. */

      for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1) { continue; }

        if (::bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0) break; /* Success */

        close(sfd);
      }

      if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not bind\n");
        exit(EXIT_FAILURE);
      }

      clientAddrLen = rp->ai_addrlen;
      memcpy(&clientAddr, rp->ai_addr, clientAddrLen);

      freeaddrinfo(result); /* No longer needed */
      socketFd_ = sfd;
      return 0;
    }
    UdpConnection::UdpConnection(const std::string& hostname,
                                 const int portNum) :
        socketFd_(-1),
        clientAddr{0}, serverAddr{0}, clientAddrLen{0}, serverAddrLen{0},
        hostname_(hostname), portNum_(portNum)
    {
    }
  }  // namespace udp
}