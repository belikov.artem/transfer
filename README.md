# transfer

transfer {OPTIONS}

    A simple client / server application for transferring files using udp
    protocol.

  OPTIONS:

      --help                            Display this help menu
      -m[mode], --mode=[mode]           receiver/transmitter - client/server
                                        mode
      -b[portnum], --bind=[portnum]     incoming / outgoing port number
      -h[hostname],
      --hostname=[hostname]             incoming / outgoing hostname
      -f[path]                          incoming path to file / outgoing folder