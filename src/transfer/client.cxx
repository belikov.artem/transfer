#include <netinet/in.h>
#include <arpa/inet.h>
#include <memory>
#include <iostream>
#include <cstring>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "transfer/client.hxx"
#include <transfer/UdpConnection.hxx>
#include <transfer/packet.hxx>

namespace transfer {
    namespace udp {
        Client::Client(const transfer::udp::Params &params) : Application(params), fileDesc{-1}, map{nullptr},
                                                              filelen{0} {}

        int Client::DoRun() {
            std::unique_ptr<transfer::udp::Connection> connection =
                    std::make_unique<transfer::udp::UdpConnection>(getParams().hostname_,
                                                                   getParams().portnum_);
            connection->connect();

            if (connection->isValid()) {
                transfer::udp::Packet packet{};
                size_t packet_num = 0;
                size_t real_file_len = 0;
                bool init_file = true;
                do {
                    memset(&packet, 0, sizeof(packet));
                    ConfirmAndRequestNewPieceOfData(connection, packet_num);

                    ReceiveDataPacket(connection, packet);

                    if (packet.frag_no == packet_num) {
                        if (init_file){
                            init_file = false;
                            filelen = packet.total_frag * transfer::udp::UdpPacket::getPacketDataSize();
                            initfile(packet.filename);

                        }
                        memcpy(reinterpret_cast<char*>(map) + packet_num*transfer::udp::UdpPacket::getPacketDataSize(), packet.data,packet.size);
                        real_file_len += packet.size;
                        packet_num++;
                    }
                    std::cout << "packetNo = " << packet.frag_no << ", size = " << packet.size << std::endl;
                } while (packet.total_frag > packet_num);
                ftruncate(fileDesc, real_file_len);

            }

            return 0;
        }

        void Client::ReceiveDataPacket(std::unique_ptr<Connection> &connection, Packet &packet) const {
            char buf[BUFFER_LEN] = {0};
            connection->read(buf, BUFFER_LEN);

            memcpy(&packet, buf, sizeof(packet));
        }

        void Client::ConfirmAndRequestNewPieceOfData(std::unique_ptr<Connection> &connection, size_t packet_num) const {
            std::string hello = "I'M READY:";
            hello += std::to_string(packet_num);

            const auto resultWrite =
                    connection->write(hello.c_str(), hello.length());
        }

        void Client::initfile(const char* filename) {
            std::string full_path = getParams().path_ + "/";
            full_path += filename;
            fileDesc = open(full_path.c_str(), O_RDWR | O_CREAT, (mode_t) 0600);
            ftruncate(fileDesc, filelen);
            lseek(fileDesc, filelen - 1, SEEK_SET);
            write(fileDesc, "", 1);
            map = mmap(0, filelen, PROT_WRITE | PROT_READ, MAP_SHARED, fileDesc, 0);
            if (map == MAP_FAILED){
                std::cerr << "map file error: " << strerror(errno) << std::endl;
            }
        }

        Client::~Client() {
            if (fileDesc != -1) {
                if (msync(map, filelen, MS_SYNC) == -1){
                    std::cerr << "cannot synchronize memory map" << std::endl;
                }
                if (munmap(map, filelen) == -1){
                    std::cerr << "cannot terminate memory map use" << std::endl;
                }
                close(fileDesc);
            }

        }
    }  // namespace udp
}