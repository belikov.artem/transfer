#ifndef CONNECTION_UDPCONNECTION_HXX
#define CONNECTION_UDPCONNECTION_HXX

#include <netinet/in.h>
#include <string>

#include "Connection.hxx"

namespace transfer
{
  namespace udp
  {
    class UdpConnection : public transfer::udp::Connection
    {
     public:
      UdpConnection(const std::string& hostname, const int portNum);
      int read(void* buf, const size_t len) override;
      int write(const void* data, const size_t len) override;
      bool isValid() const override;
      void connect() override;
      ~UdpConnection() override;
      int bind() override;

     private:
      int socketFd_;
      struct sockaddr_in clientAddr;
      struct sockaddr_in serverAddr;
      socklen_t clientAddrLen;
      socklen_t serverAddrLen;
      std::string hostname_;
      int portNum_;
    };
  }  // namespace udp
}  // namespace transfer

#endif  // CONNECTION_UDPCONNECTION_HXX
