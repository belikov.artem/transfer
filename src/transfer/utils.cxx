#include <transfer/utils.hxx>

namespace transfer
{
  namespace utils
  {
    Mode fromStringToMode(const std::string mode)
    {
      if (mode.compare("receiver") == 0) { return Mode::RECEIVER; }
      if (mode.compare("transmitter") == 0) { return Mode::TRANSMITTER; }
      return Mode::UNKNOWN;
    }
  }  // namespace utils
}