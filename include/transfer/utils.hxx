#ifndef UDP_TRASMITTER_RECIEVER_UTILS_HXX
#define UDP_TRASMITTER_RECIEVER_UTILS_HXX
#include <string>
namespace transfer
{
  namespace utils
  {
    using PortType = uint16_t;
    using PathType = std::string;
    using HostnameType = std::string;

    enum Mode { UNKNOWN = 0, TRANSMITTER = 1, RECEIVER = 2 };

    Mode fromStringToMode(const std::string mode);
  }  // namespace utils
}  // namespace transfer
#endif  // UDP_TRASMITTER_RECIEVER_UTILS_HXX
