#ifndef UDP_TRASMITTER_RECIEVER_APPLICATION_HXX
#define UDP_TRASMITTER_RECIEVER_APPLICATION_HXX
#include <cstdint>
#include <string>

namespace transfer
{
  namespace utils
  {
    using PortType = uint16_t;
    using PathType = std::string;
    using HostnameType = std::string;

    enum Mode { UNKNOWN = 0, TRANSMITTER = 1, RECEIVER = 2 };

    Mode fromStringToMode(const std::string mode);
  }  // namespace utils

  namespace udp
  {
    struct Params {
      utils::PortType portnum_;
      utils::HostnameType hostname_;
      utils::PathType path_;
      explicit Params(const utils::PortType portnum,
                      const utils::HostnameType hostname,
                      const utils::PathType path) :
          portnum_(portnum),
          hostname_(hostname), path_(path)
      {
      }
    };

    struct IApplication {
      virtual int DoRun() = 0;
      virtual ~IApplication() = default;
    };

    class Application : public IApplication
    {
     public:
      explicit Application(const Params& params);

     protected:
      const Params& getParams() const;

     private:
      Params params_;
    };

  }  // namespace udp
}  // namespace transfer

#endif  // UDP_TRASMITTER_RECIEVER_APPLICATION_HXX
