#ifndef UDP_TRASMITTER_RECIEVER_PACKET_HXX
#define UDP_TRASMITTER_RECIEVER_PACKET_HXX

#include <cstdint>
#include <cstdlib>

namespace transfer {
    namespace udp {
        const int MAX_FILE_NAME_SIZE = 100;
        const int MAX_DATA_SIZE = 1000;
        struct Packet {
            size_t total_frag;
            size_t frag_no;
            size_t size;
            char filename[MAX_FILE_NAME_SIZE];
            char data[MAX_DATA_SIZE];
        };

        class UdpPacket {
        public:

            UdpPacket(const size_t filesize, const char *filename);

/**
           * @brief создать
           * @param data
           * @return
           */
            transfer::udp::Packet create(const char *data, const size_t length, const size_t frag_no);

            /**
             * @brief извлечь пакет из потока
             * @param data
             */
            void extract(const char *data, const size_t length);

            static constexpr size_t getPacketDataSize()
            {
              return MAX_PACKET_DATA_SIZE;
            }

           private:
            const size_t total_frag_;
            size_t  frag_no_;
            char filename_[MAX_FILE_NAME_SIZE];
            static constexpr size_t MAX_PACKET_DATA_SIZE = MAX_DATA_SIZE;
        };
    }  // namespace udp
}  // namespace transfer
#endif  // UDP_TRASMITTER_RECIEVER_PACKET_HXX