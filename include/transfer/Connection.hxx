#ifndef CONNECTION_CONNECTION_HXX
#define CONNECTION_CONNECTION_HXX

#include <cstdlib>
namespace transfer
{
  namespace udp
  {
    class Connection
    {
     public:
      virtual int read(void *buf, const size_t len) = 0;
      virtual int write(const void *data, const size_t len) = 0;
      virtual bool isValid() const = 0;
      virtual void connect() = 0;
      virtual int bind() = 0;
      virtual ~Connection() = default;
    };
  }  // namespace udp
}  // namespace transfer

#endif  // CONNECTION_CONNECTION_HXX
