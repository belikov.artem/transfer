#include <memory>
#include <arpa/inet.h>
#include <iostream>
#include "transfer/application.hxx"
#include "transfer/server.hxx"
#include "transfer/client.hxx"

namespace transfer
{
  namespace udp
  {
    Application::Application(const transfer::udp::Params& params) :
        params_(params)
    {
    }

    const Params& Application::getParams() const
    {
      return params_;
    }

  }  // namespace udp
}  // namespace transfer