#include <cstring>
#include "transfer/packet.hxx"

transfer::udp::Packet transfer::udp::UdpPacket::create(const char *data, const size_t length, const size_t frag_no) {
    transfer::udp::Packet packet;
    memcpy(packet.filename, this->filename_, MAX_FILE_NAME_SIZE);
    packet.frag_no = frag_no;
    packet.total_frag = this->total_frag_;
    packet.size = length > MAX_DATA_SIZE ? MAX_DATA_SIZE : length;
    memcpy(packet.data,data, packet.size);
    return packet;
}

void transfer::udp::UdpPacket::extract(const char *data, const size_t length) {

}

transfer::udp::UdpPacket::UdpPacket(const size_t filesize, const char *filename) : total_frag_(filesize / MAX_DATA_SIZE + 1),
                                                                                           frag_no_{0},
                                                                                           filename_{} {
    strncpy(filename_, filename, MAX_FILE_NAME_SIZE - 1);
}