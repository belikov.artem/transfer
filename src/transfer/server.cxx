#include <netinet/in.h>
#include <arpa/inet.h>
#include <memory>
#include <iostream>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <transfer/UdpConnection.hxx>
#include <cstring>
#include <transfer/packet.hxx>
#include <sstream>
#include <spdlog/spdlog.h>
#include "transfer/server.hxx"

namespace transfer {
    namespace udp {
        Server::Server(const transfer::udp::Params &params) : Application(params), fileDesc{-1}, filesize{0} {}

        void *Server::readfile() {
            fileDesc = open(getParams().path_.c_str(), O_RDONLY, S_IRUSR | S_IWUSR);
            if (fileDesc == -1) {
                spdlog::critical("cannot open file {0}, with error {1}", getParams().path_.c_str(), strerror(errno));
                throw std::runtime_error(std::string("cannot open file :") + getParams().path_.c_str());
                return nullptr;
            }
            struct stat sb;
            if (fstat(fileDesc, &sb) == -1) {
                spdlog::critical("cannot get stat for file {0}, with error {1}", getParams().path_.c_str(),
                                 strerror(errno));
                throw std::runtime_error(std::string("cannot get stat for file :") + getParams().path_.c_str());
                return nullptr;
            }

            filesize = sb.st_size;

            auto pMemory = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fileDesc, 0);
            if (pMemory == MAP_FAILED) {
                spdlog::critical("cannot map file %s, to memory with error %s", getParams().path_.c_str(),
                                 strerror(errno));
                throw std::runtime_error(std::string("cannot map to memory file :") + getParams().path_.c_str());
            }

            return pMemory;
        }

        int Server::DoRun() {

            void *data = readfile();
            std::unique_ptr<transfer::udp::Connection> connection =
                    std::make_unique<transfer::udp::UdpConnection>(getParams().hostname_,
                                                                   getParams().portnum_);
            connection->bind();

            if (connection->isValid()) {
                const int BUFFER_LEN = 1500;
                char buf[BUFFER_LEN] = {};
                size_t packet_num = 0;
                transfer::udp::UdpPacket udp_packet{filesize, getParams().path_.c_str()};

                std::cout << "filesize: " << filesize << std::endl;
                std::cout << "packet count: " << filesize / transfer::udp::UdpPacket::getPacketDataSize() << std::endl;
                while (packet_num < filesize / transfer::udp::UdpPacket::getPacketDataSize()) {
                    memset(buf, 0, BUFFER_LEN);
                    const auto resultRead = connection->read(buf, BUFFER_LEN);
                    if (resultRead > 0) {
                        const int HELLO_MESSAGE_LEN = strlen("I'M READY:");
                        if (std::string("I'M READY:") == std::string(buf, HELLO_MESSAGE_LEN)) {
                            std::cout << buf << std::endl;
                            std::stringstream ss(std::string(buf + HELLO_MESSAGE_LEN));
                            ss >> packet_num;
                            std::cout << "packet_num = " << packet_num << std::endl;
                        }
                    }
                    transfer::udp::Packet packet = udp_packet.create(
                            reinterpret_cast<const char *>(data) + packet_num * transfer::udp::UdpPacket::getPacketDataSize(), filesize - packet_num * transfer::udp::UdpPacket::getPacketDataSize(),
                            packet_num);
                    const auto resultWrite =
                            connection->write(reinterpret_cast<const void *>(&packet), sizeof(packet));
                    std::cout << "ok: send packet:" << packet.size << std::endl;
                }
            }
            return 0;
        }

        Server::~Server() {
            if (fileDesc != -1) {
                close(fileDesc);
            }
        }
    }  // namespace udp
}