#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>
#include <args/args.hxx>
#include <iostream>
#include <transfer/application.hxx>
#include "transfer/server.hxx"
#include "transfer/client.hxx"

void debugCommandLineFlags(
    args::ValueFlag<std::string> &mode,
    args::ValueFlag<transfer::utils::PortType> &portNumber,
    args::ValueFlag<transfer::utils::HostnameType> &hostname,
    args::ValueFlag<transfer::utils::PathType> &path);
/*

transfer -h <host> -b <port> -f <file> -m <mode>

mode - может быть либо receiver либо transmitter.

В режиме transmitter:

-h - указываем хост ресивера

-b - указываем порт ресивера

-f - указываем какой файл надо послать

В режиме receiver:

-h - указываем хост на который байдимся

-b - указываем port bind

-f - указываем директорию куда складывать файлы
 */

void readFile(const std::string file) {}

std::unique_ptr<transfer::udp::IApplication> Factory(
    const transfer::utils::Mode mode, const transfer::udp::Params &params)
{
  switch (mode) {
    case transfer::utils::Mode::RECEIVER:
      return std::make_unique<transfer::udp::Client>(params);
    case transfer::utils::Mode::TRANSMITTER:
      return std::make_unique<transfer::udp::Server>(params);
    default:
      throw std::runtime_error(std::string("unknown mode"));
  }
}

int main(int argc, char **argv)
{
  args::ArgumentParser parser(
      "A simple client / server application for transferring files using udp "
      "protocol.");
  args::HelpFlag help(parser, "help", "Display this help menu", {"help"});
  args::ValueFlag<std::string> mode(parser,
                                    "mode",
                                    "receiver/transmitter - client/server mode",
                                    {'m', "mode"});
  args::ValueFlag<transfer::utils::PortType> portNumber(
      parser, "portnum", "incoming / outgoing port number", {'b', "bind"});
  args::ValueFlag<transfer::utils::HostnameType> hostname(
      parser, "hostname", "incoming / outgoing hostname", {'h', "hostname"});
  args::ValueFlag<transfer::utils::PathType> path(
      parser, "path", "incoming path to file / outgoing folder", {'f'});

  try {
    parser.ParseCLI(argc, argv);

    if (!portNumber || !hostname || !mode || !path) {
      throw args::ValidationError(
          "Must be a set portnumber, hostname, mode and path");
    }
    const transfer::utils::Mode loMode =
        transfer::utils::fromStringToMode(args::get(mode));
    if (loMode == transfer::utils::Mode::UNKNOWN) {
      throw args::ValidationError("Mode must be a receiver or transmitter");
    }

      /*debugCommandLineFlags(mode, portNumber, hostname, path);*/

    const transfer::udp::Params params{
        args::get(portNumber), args::get(hostname), args::get(path)};

    // spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");
    spdlog::set_level(spdlog::level::debug);
    try {
      auto file_logger = spdlog::basic_logger_mt(
              "basic_logger", "transfer.log");
      spdlog::set_default_logger(file_logger);
    } catch (const spdlog::spdlog_ex &ex) {
      std::cerr << "Log to file init failed: " << ex.what() << std::endl;
    }
    spdlog::debug("This message should be displayed..");
    /** --TODO
     *  реализовать ротирование журналов лога
     *  реализовать логирование в системный каталог /var/log
     *  реализовать установку параметров логирования через файл конфигурации
     *  логировать в /var/log
     **/
    auto application = Factory(loMode, params);

    return application->DoRun();

  } catch (args::Help) {
    std::cout << parser;
    return EXIT_SUCCESS;
  } catch (args::ParseError e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    return EXIT_FAILURE;
  } catch (args::ValidationError e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    return EXIT_FAILURE;
  } catch (const std::runtime_error &e) {
    spdlog::critical(e.what());
    std::cerr << e.what() << std::endl;
      return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
void debugCommandLineFlags(
    args::ValueFlag<std::string> &mode,
    args::ValueFlag<transfer::utils::PortType> &portNumber,
    args::ValueFlag<transfer::utils::HostnameType> &hostname,
    args::ValueFlag<transfer::utils::PathType> &path)
{
  std::cout << "p: " << args::get(portNumber) << std::endl;
  std::cout << "h: " << args::get(hostname) << std::endl;
  std::cout << "m: " << args::get(mode) << std::endl;
  std::cout << "f: " << args::get(path) << std::endl;
}